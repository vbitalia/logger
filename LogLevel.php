<?php
/**
 * LogLevel class file.
 *
 * Class constants used to know the message type.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package VB\Logger
 * @copyright 2015 VB Italia Srl
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @author Mattia Migliorini <mattia@vbitalia.net>
 * @license GPLv2+
 * @since 1.0.0
 */

namespace VB\Logger;
use Psr\Log\LogLevel as PsrLogLevel;

/**
 * Class of constants.
 *
 * This class contains all the constants corresponding to certain message types.
 *
 * @package VB\Logger
 * @author Gianluca Merlo <gianluca.merlo@vbitalia.net>
 * @author Mattia Migliorini <mattia@vbitalia.net>
 * @since 1.0.0
 * @version 1.0.0
 *
 * @codeCoverageIgnore
 */
class LogLevel extends PsrLogLevel {
  const DEPRECATED = 'deprecated';
}
