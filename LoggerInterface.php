<?php
/**
 * Logger Interface file.
 *
 * This document extends PSR-3.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package VB\Logger
 * @copyright 2015 VB Italia Srl
 * @author Mattia Migliorini <mattia@vbitalia.net>
 * @license GPLv2+
 * @since 1.0.0
 */

namespace VB\Logger;
use Psr\Log\LoggerInterface as PsrLoggerInterface;

/**
 * Logger Interface.
 *
 * Defines additional standards for Logger over PSR-3.
 *
 * @package VB\Logger
 * @author Mattia Migliorini <mattia@vbitalia.net>
 * @since 1.0.0
 * @version 1.2.0
 */
interface LoggerInterface extends PsrLoggerInterface {
  /**
   * Notify use of deprecated element.
   *
   * @since 1.1.0
   * @access public
   *
   * @param string $element Deprecated element.
   * @param string $version Version since element is deprecated.
   * @param string $replacement Optional. Element to use instead. Default null.
   */
  public function deprecated($element, $version, $replacement = null);

  /**
   * Disable logging.
   *
   * @since 1.2.0
   * @access public
   * @static
   */
  public static function disable();

  /**
   * Enable logging.
   *
   * @since 1.2.0
   * @access public
   * @static
   */
  public static function enable();
}
