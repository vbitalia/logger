<?php
/**
 * Logger Exception Interface File.
 *
 * This MUST be implemented by every Logger Exception.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package VB\Logger
 * @copyright 2015 VB Italia Srl
 * @license GPLv2+
 * @since 1.2.1
 */

namespace VB\Logger;

/**
 * Logger Exception.
 *
 * MUST be implemented by all exceptions in this library.
 *
 * @package VB\Logger
 * @author Mattia Migliorini <mattia@vbitalia.net>
 * @since 1.2.1
 * @version 1.0.0
 */
interface LoggerException {
  
}
