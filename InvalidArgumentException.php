<?php
/**
 * Logger Invalid Argument Exception File.
 *
 * Copyright (C) 2015 VB Italia Srl
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  See LICENSE.md
 *
 * @package VB\Logger
 * @copyright 2015 VB Italia Srl
 * @license GPLv2+
 * @since 1.2.1
 */

namespace VB\Logger;

/**
 * Logger Invalid Argument Exception.
 *
 * @package VB\Logger
 * @author Mattia Migliorini <mattia@vbitalia.net>
 * @since 1.2.1
 * @version 1.0.0
 */
class InvalidArgumentException extends \Exception implements LoggerException {
  /**
   * Raise exception for type mismatch.
   *
   * @since 1.0.0
   * @access public
   * @static
   *
   * @param string $variable Variable name.
   * @param mixed $value Variable value.
   * @param string $expected Expected type.
   * @return InvalidArgumentException
   */
  public static function typeMismatch($variable, $value, $expected) {
    return new static(sprintf(
      '%1$s value expected for $%2$s but %3$s given',
      ucfirst($expected),
      $variable,
      gettype($value)
    ));
  }
}
